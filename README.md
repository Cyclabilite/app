Cyclabilité est maintenant hébergé sur gitlab
=============================================

Le projet se trouve maintenant hébergé sur gitlab :
- https://gitlab.com/cyclabilite/app
- git@gitlab.com:cyclabilite/app.git
- https://gitlab.com/cyclabilite/app.git


The project has moved to gitlab
===============================

Le project is now hosted on gitlab :
- https://gitlab.com/cyclabilite/app
- git@gitlab.com:cyclabilite/app.git
- https://gitlab.com/cyclabilite/app.git